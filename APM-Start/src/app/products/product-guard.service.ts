import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';

@Injectable()
export class ProductGuardService implements CanActivate {

  constructor(private _router: Router) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    let id = +route.url[1].path; //route.url is "products/10" so only want index 1
    if (isNaN(id) || id < 1) {
      alert("Invalid product id");
      this._router.navigate(['/products']);
      return false;
    }
    return true;
  }

}
